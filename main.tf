/*
This is a test server definition for GCE+Terraform for GH-9564
*/

provider "google" {
	credentials = "${file("heroic-mechanic-264406-766d914d7d9b.json")}"
	project = "heroic-mechanic-264406"
	region = "us-east1"
}

resource "google_compute_firewall" "gh-9564-firewall-externalssh" {
  name    = "gh-9564-firewall-externalssh"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["0-65535"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["externalssh"]
}

resource "google_compute_instance" "dev1" {
  name         = "gcp-rhel7-dev1-tf"
  machine_type = "g1-small"
  zone         = "us-central1-a"
  tags         = ["externalssh"]

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1804-lts"
    }
  }

  network_interface {
    network = "default"

    access_config {
      # Ephemeral
    }
  }

  provisioner "remote-exec" {
    connection {
      host        = self.network_interface[0].access_config[0].nat_ip
      type        = "ssh"
      user        = "srikarsai94"
      agent       = "false"
      private_key = "${file("~/.ssh/my-ssh-key")}"
    }

    inline = [
      "touch /tmp/temp.txt",
      "sudo dpkg -l selinux*",
      "sudo apt update && sudo apt upgrade -y",
      "sudo apt-get install -y autoconf gcc libc6 make wget unzip apache2 php libapache2-mod-php7.2 libgd-dev",
      "cd /tmp",
      "wget -O nagioscore.tar.gz https://github.com/NagiosEnterprises/nagioscore/archive/nagios-4.4.5.tar.gz",
      "tar xzf nagioscore.tar.gz",
      "cd /tmp/nagioscore-nagios-4.4.5/",
      "sudo ./configure --with-httpd-conf=/etc/apache2/sites-enabled",
      "sudo make all",
      "sudo make install-groups-users",
      "sudo usermod -a -G nagios www-data",
      "sudo make install",
      "sudo make install-daemoninit",
      "sudo make install-commandmode",
      "sudo make install-config",
      "sudo make install-webconf",
      "sudo a2enmod rewrite",
      "sudo a2enmod cgi",
      "sudo ufw allow Apache",
      "sudo ufw reload",
      "sudo htpasswd -s -c -b /usr/local/nagios/etc/htpasswd.users nagiosadmin nagios",
      "sudo systemctl restart apache2.service",
      "sudo systemctl start nagios.service",
    ]
  }

  # Ensure firewall rule is provisioned before server, so that SSH doesn't fail.
  depends_on = ["google_compute_firewall.gh-9564-firewall-externalssh"]

  service_account {
    scopes = ["compute-ro"]
  }

  metadata = {
    ssh-keys = "srikarsai94:${file("~/.ssh/my-ssh-key.pub")}"
  }
}
